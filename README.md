Investment Tracker
==================

The goal of this project is to know exactly how much gold you have invested in
any item at any time. I recognize that there is value in farming your own items,
but I don't have a way to calculate that value.


This is done through tracking these specific actions. Checked items have been implemented.

- [x] Track items purchased on the Auction House
- [x] Track items sold on the Auction House
- [x] Track Auction House deposit
- [x] Transfer value when crafting items
- [x] Transfer value when converting items ([Lesser Mystic Essence])
- [ ] Items purchased from vendors
- [ ] Items sold to vendors
- [ ] Track items sent to you with a COD
- [ ] Track items you COD to others
- [ ] Track trades with other players
- [ ] Milling
- [ ] Prospecting
- [ ] Disenchanting

Value Conversions
-----------------

For example, lets say you purchased:

* [Greater Mystic Essence]x2 on the AH for 1g50s each.
* [Star Wood]x1 for 45s
* [Vision Dust]x1 for 3g

If you craft a [Greater Mystic Wand] then you transfer the value of its reagents
(1 of each item above) and now have 4g95s invested in [Greater Mystic Wand]s.

Now you post the [Greater Mystic Wand] on the AH for 48 hours. The vendor price
is 52s63c, which makes the deposit on a faction AH 7s90c. Now you have 60s53c
invested in [Greater Mystic Wand].

You still have a [Greater Mystic Essence] left, so you convert it to a [Lesser
Mystic Essence]. Now you have 3 [Lesser Mystic Essence] that you have 50s each
invested in.
