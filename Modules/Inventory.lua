-- GLOBALS: TSMAPI
local InvestmentTracker = select(2, ...)
local Inventory = InvestmentTracker:NewModule("Inventory")

function Inventory:OnInitialize()
    self:Debug('Inventory?')
    self:RegisterEvent("BAG_OPEN", 'ScanBags')
    self:RegisterEvent("BANKFRAME_OPENED", 'ScanBank')
end

function Inventory:ScanBags()
    self:Debug('ScanBags')
    for _, _, itemString in TSMAPI.Inventory:BagIterator() do
        InvestmentTracker:GetInvested(itemString)
    end
end

function Inventory:ScanBank()
    for _, _, itemString in TSMAPI.Inventory:BankIterator(false, false, false, true) do
        InvestmentTracker:GetInvested(itemString)
    end
end
