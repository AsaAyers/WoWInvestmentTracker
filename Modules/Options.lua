-- GLOBALS: TSMAPI
local InvestmentTracker = select(2, ...)
local Options = InvestmentTracker:NewModule("Options")

function Options:LoadTooltipOptions(container, options)
	local page = {
		{
			type = "SimpleGroup",
			layout = "Flow",
			fullHeight = true,
			children = {
				{
					type = "CheckBox",
					label = "Show invested in item tooltips",
					settingInfo = { options, "invested" },
					tooltip = "If checked, the amount of money you have invested in an item type will show up in that item's tooltip"
				},
			},
		},
	}

	TSMAPI.GUI:BuildOptions(container, page)
end
