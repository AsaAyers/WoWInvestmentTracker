-- GLOBALS: UnitCastingInfo, TSMAPI
local InvestmentTracker = select(2, ...)
local Convertables = InvestmentTracker:NewModule("Convertables", 'AceBucket-3.0', 'AceEvent-3.0', 'AceTimer-3.0')


function Convertables:OnInitialize()
    self:RegisterEvent("UNIT_SPELLCAST_SENT")
    self:RegisterEvent("UNIT_SPELLCAST_START")
    self.watch_handle = self:RegisterBucketEvent({"BAG_UPDATE", "PLAYER_MONEY", "UNIT_INVENTORY_CHANGED"}, 0.3, "Scan")
end

function Convertables:UNIT_SPELLCAST_SENT(event, casting, spellName)
    -- Convertables don't trigger this event
    if casting == 'player' then
        self.lastScan = self:ScanBags()
    end
end

function Convertables:UNIT_SPELLCAST_START(event, casting, spellName)
    -- Convertables don't trigger this event
    if casting == 'player' then
        self.lastScan = nil
    end
end

function Convertables:Scan()
    if self.lastScan ~= nil then
        self:CompareScans(self.lastScan, self:ScanBags())
        self.lastScan = nil
    end
end
