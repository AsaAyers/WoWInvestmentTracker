-- GLOBALS: GetInboxNumItems, GetInboxInvoiceInfo, GetInboxHeaderInfo, GetInboxItemLink
local InvestmentTracker = select(2, ...)
local Mail = InvestmentTracker:NewModule("Mail", 'AceHook-3.0')
local Debug = InvestmentTracker.Debug

function Mail:OnInitialize()
    self:RegisterEvent("MAIL_SHOW")
end

function Mail:MAIL_SHOW()
    self:RegisterEvent("MAIL_CLOSED")
    self:RegisterEvent("MAIL_INBOX_UPDATE")

    Mail:RawHook("TakeInboxItem", function(...) Mail:Scan("TakeInboxItem", ...) end, true)
    Mail:RawHook("TakeInboxMoney", function(...) Mail:Scan("TakeInboxMoney", ...) end, true)
    Mail:RawHook("AutoLootMailItem", function(...) Mail:Scan("AutoLootMailItem", ...) end, true)

    self.lastScan = self:Scan()
end

function Mail:MAIL_CLOSED()
    self:UnhookAll()
    self:UnregisterEvent("MAIL_CLOSED")
    self:UnregisterEvent("MAIL_INBOX_UPDATE")
end

function Mail:MAIL_INBOX_UPDATE()
end

function Mail:Scan(hookedMethod, mailIndex, subIndex)
    if mailIndex == nil then return end

    local invoiceType, itemName, buyer, bid, _, _, ahcut, _, _, _, quantity = GetInboxInvoiceInfo(mailIndex)
    local sender, msgSubject, msgMoney, msgCOD, daysLeft, msgItem, _, _, msgText, _, isGM = select(3, GetInboxHeaderInfo(mailIndex))
    if invoiceType == "buyer" then
        local link = GetInboxItemLink(mailIndex, 1)
        InvestmentTracker:AddInvested(link, bid)
    elseif invoiceType == "seller" then
        local link = InvestmentTracker:FindByName(itemName)
        if link ~= nil then
            InvestmentTracker:SubtractInvested(link, msgMoney)
        end
    end

    return Mail.hooks[hookedMethod](mailIndex, subIndex)
end
