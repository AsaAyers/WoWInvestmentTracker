-- GLOBALS: GetFirstTradeSkill, GetTradeSkillInfo, GetNumTradeSkills, UnitCastingInfo
-- GLOBALS: GetTradeSkillNumReagents, GetTradeSkillReagentInfo, GetTradeSkillItemLink
-- GLOBALS: GetTradeSkillReagentItemLink, GetTradeSkillNumMade
local InvestmentTracker = select(2, ...)
local Crafting = InvestmentTracker:NewModule("Crafting")

function Crafting:OnInitialize()
    self:ToggleEvents("TRADE_SKILL_SHOW", "TRADE_SKILL_CLOSE")
    -- self:RegisterEvent("TRADE_SKILL_SHOW")
end

function Crafting:TRADE_SKILL_SHOW()
    self:RegisterEvent("UNIT_SPELLCAST_START")
    self:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
end

local function findTradeskillIndex(name)
    for i = GetFirstTradeSkill(), GetNumTradeSkills() do
        local tsName = GetTradeSkillInfo(i)

        if tsName == name then
            return i
        end
    end
end

local CeruleanPigment = "\124cffffffff\124Hitem:114931:0:0:0:0:0:0:0:0:0:0\124h[Cerulean Pigment]\124h\124r"
function GetCrafted(id)
    local itemLink = GetTradeSkillItemLink(id)
    local numMade = GetTradeSkillNumMade(id)

    local spellString = string.match(itemLink, "enchant[%-?%d:]+")
    if
        spellString == 'enchant:190381' or -- Mass Mill Frostweed
        spellString == 'enchant:190382' or -- Mass Mill Fireweed
        spellString == 'enchant:190383' or -- Mass Mill Gorgrond Flytrap
        spellString == 'enchant:190384' or -- Mass Mill Starflower
        spellString == 'enchant:190385' or -- Mass Mill Nagrand Arrowbloom
        spellString == 'enchant:190386'    -- Mass Mill Talador Orchid
    then
        itemLink = CeruleanPigment
        numMade = 8
    end

    return {
        [itemLink] = numMade
    }
end

function Crafting:UNIT_SPELLCAST_START(event, casting, spellName)
    if casting ~= "player" then return end
    local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("player")
    if isTradeSkill == false then return end

    local id = findTradeskillIndex(spellName)

    if id == nil then return end

    local numReagents = GetTradeSkillNumReagents(id)

    local reagents = {}
    for i=1, numReagents, 1 do
        local reagentName, reagentTexture, reagentCount, playerReagentCount = GetTradeSkillReagentInfo(id, i);
        local link = GetTradeSkillReagentItemLink(id, i)
        reagents[link] = reagentCount
    end

    local crafted = GetCrafted(id)

    self.crafting = {
		crafted = crafted,
        reagents = reagents,
    }
end

function Crafting:UNIT_SPELLCAST_SUCCEEDED(event, casting, spellName, _, spellIdCounter, spellId)
    if casting ~= "player" then return end
    local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo("player")
    if isTradeSkill == false then return end

    if self.crafting == nil then
        if endTime ~= nil then
            self:Print("Unexpected error: Unable to convert value for cast:", spellName)
        end
        return
    end

    InvestmentTracker:ConvertValue(self.crafting.reagents, self.crafting.crafted)
    self.crafting = nil
end

--@debug@
-- GLOBALS: _FakeCraft
function _FakeCraft(index)
    if Crafting.crafting == nil then
        Crafting:Print("Start crafting an item and cancel it first")
        return
    end

    InvestmentTracker:ConvertValue(Crafting.crafting.reagents, Crafting.crafting.crafted)

end
--@end-debug@
