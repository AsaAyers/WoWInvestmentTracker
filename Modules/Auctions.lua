-- GLOBALS: CalculateAuctionDeposit, GetContainerItemLink
local InvestmentTracker = select(2, ...)
local Auctions = InvestmentTracker:NewModule("Auctions", 'AceHook-3.0')
local Debug = InvestmentTracker.Debug


function Auctions:OnInitialize()
	self:ToggleEvents("AUCTION_HOUSE_SHOW", "AUCTION_HOUSE_CLOSED")
end

function Auctions:AUCTION_HOUSE_SHOW()
	self:RegisterEvent("ITEM_LOCKED")
	self:RawHook("StartAuction", 'StartAuction', true)
end

function Auctions:AUCTION_HOUSE_CLOSED()
    self:UnhookAll()
end

function Auctions:ITEM_LOCKED(event, bagID, slot)
    if bagID ~= nil and slot ~= nil and bagID >= 0 and slot >= 0 then
        self.sellingItem = GetContainerItemLink(bagID, slot)
    end
end

function Auctions:StartAuction(minBid, buyoutPrice, runTime, stackSize, numStacks)
    if self.sellingItem == nil then
        self:Debug("Unable to determine which item this auction is posting")
    end
    local deposit = CalculateAuctionDeposit(runTime, stackSize, numStacks)
    InvestmentTracker:AddInvested(self.sellingItem, deposit)
    self.sellingItem = nil

    return self.hooks.StartAuction(minBid, buyoutPrice, runTime, stackSize, numStacks)
end
