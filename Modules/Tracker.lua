local InvestmentTracker = select(2, ...)
InvestmentTracker.Tracker = {}
local Tracker = InvestmentTracker.Tracker

local function MoneyFormat(value, moneyCoins)
    return TSMAPI:MoneyToString(value, "|cffffffff", "OPT_PAD", moneyCoins and "OPT_ICON" or nil)
end

--@debug@
function Tracker:new(o)
    o = o or {}   -- create object if user does not provide one
    setmetatable(o, self)
    self.__index = self

    o.db = {
        global = {
            itemNames = {}
        },
        factionrealm = {
            items = {},
        },
    }
    o.owned = o.owned or {}
    function o:GetNumOwned(itemString)
        local itemString = TSMAPI.Item:ToBaseItemString(itemString, true)
        for ownedString, count in pairs(o.owned) do
            local ownedString = TSMAPI.Item:ToBaseItemString(ownedString, true)
            if ownedString == itemString then return count end
        end
        return 0
    end

    return o
end
--@end-debug@

function Tracker:ScanBags()
    local items = {}
    for _, _, itemString, qty in TSMAPI.Inventory:BagIterator(true) do
        if items[itemString] == nil then
            items[itemString] = 0
        end
        items[itemString] = items[itemString] + qty
    end
    return items
end

local function diff(before, after)
    local diff = {}
    local change
    for itemString, qty in pairs(after) do
        if before[itemString] == nil then
            diff[itemString] = after[itemString]
        else
            change = after[itemString] - before[itemString]
            if change ~= 0 then
                diff[itemString] = change
            end
        end
    end
    for itemString, qty in pairs(before) do
        if after[itemString] == nil then
            diff[itemString] = 0 - qty
        else
            change = after[itemString] - before[itemString]
            if change ~= 0 then
                diff[itemString] = change
            end
        end
    end
    return diff
end

function Tracker:CompareScans(lastScan, current)
    local diff = diff(lastScan, current)

    local reagents = {}
    local targets = {}
    local reagentCount = 0
    local targetCount = 0
    for itemString, change in pairs(diff) do
        local link = TSMAPI.Item:ToItemLink(itemString)

        if change < 0 then
            reagents[link] = 0 - change
            reagentCount = reagentCount + 1
        else
            targets[link] = change
            targetCount = targetCount + 1
        end
    end

    if targetCount > 0 and reagentCount > 0 then
        return reagents, targets
    end
end

function Tracker:ConvertValue(fromItems, toItems)
    local totalValue = 0
    for link, count in pairs(fromItems) do
        local _, totalInvested = self:GetInvested(link)

        local numOwned = count + self:GetNumOwned(link)
        local investedEach = totalInvested / numOwned

        local reagentValue = count * investedEach
        totalValue = totalValue + reagentValue
        if reagentValue > 0 then
            self:SubtractInvested(link, reagentValue)
        end
    end

    local destination = ""
    local newItems = 0
    for link, count in pairs(toItems) do
        destination = destination .. link
        newItems = newItems + count
    end

    if totalValue > 0 then
        self:Log('Transferring value to:', destination)

        local valuePerItem = totalValue / newItems
        for link, count in pairs(toItems) do
            self:AddInvested(link, valuePerItem * count)
        end
    else
        self:Log('No value to transfer for:', destination)
    end
end

function Tracker:GetName()
    return self.name
end

function Tracker:GetNumOwned(itemString)
    return TSMAPI.Inventory:GetTotalQuantity(itemString)
end

function Tracker:GetInfo(itemString)
    print('REAL:GetInfo', itemString)
    return TSMAPI.Item:GetInfo(itemString)
end

function Tracker:GetInvested(link)
    local itemString = TSMAPI.Item:ToBaseItemString(link, true)

    if self.db.factionrealm.items[itemString] == nil then
        local name = self:GetInfo(itemString)
        self:Log('string/name', link, itemString, name)
        self.db.global.itemNames[name] = itemString

        local total = 0 -- AccountingInvested(itemString)
        if total < 0 then total = 0 end
        self.db.factionrealm.items[itemString] = total
    end
    local totalInvested = self.db.factionrealm.items[itemString]

    local numOwned = self:GetNumOwned(itemString)
    self:Log('numOwned', link, numOwned)

    local investedEach
    -- I'm not sure what should happen if you run out of an item but still have
    -- gold invested.
    if (numOwned > 0) then
        investedEach = totalInvested / numOwned
    else
        investedEach = totalInvested
    end

    return investedEach, totalInvested, itemString
end

function Tracker:AddInvested(link, value)
    local _, totalInvested, itemString = self:GetInvested(link)
    self:Log('Adding', MoneyFormat(value), 'to', link)

    self.db.factionrealm.items[itemString] = totalInvested + value
end

function Tracker:SubtractInvested(link, value)
    local _, totalInvested, itemString = self:GetInvested(link)

    if value > totalInvested then
        value = totalInvested
    end

    if value > 0 then
        self:Log('Subtracting', MoneyFormat(value), 'from', link)

        local total = totalInvested - value

        self.db.factionrealm.items[itemString] = total
    elseif value < 0 then
        self:Log('Adding', MoneyFormat(0 - value), 'to', link)
        local total = totalInvested - value

        self.db.factionrealm.items[itemString] = total
    end
end
