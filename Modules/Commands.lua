-- GLOBALS: TSMAPI
local InvestmentTracker = select(2, ...)
local Commands = InvestmentTracker:NewModule("Commands")


function Commands:OnInitialize()
    self:RegisterChatCommand("it", "SlashProcessor")
end

function Commands:SlashProcessor(str)
    local arg1 = self:GetArgs(str, 1, 1, string)

    if arg1 == "investments" then
        self:LogInvestments(str)
    elseif arg1 == "set" then
        self:SetInvested(str)
    else
        self:Print('ERROR: Invalid command', arg1)
    end
end

local function MoneyFormat(value)
    return TSMAPI:MoneyToString(value, "|cffffffff", "OPT_PAD", "OPT_ICON")
end

function Commands:LogInvestments()
    local investements = {}
    local i = 1

    for itemString, invested in pairs(InvestmentTracker.db.factionrealm.items) do
        if invested > 0 then
            local link = TSMAPI.Item:ToItemLink(itemString)
            investements[link] = invested
        end
    end

    local function sorter(a, b)
        self:Debug(a, b)

        return true
    end

    table.sort(investements, sorter)
    for link, invested in pairs(investements) do
        self:Print(MoneyFormat(invested), link)
    end
end

function Commands:SetInvested(str)
    local set, link, amount = self:GetArgs(str, 3, 1, string)
    local itemString = TSMAPI.Item:ToBaseItemString(link)

    if itemString == nil then
        self:Print('usage: add [item link] amount')
        return
    end
    local numOwned = TSMAPI.Inventory:GetTotalQuantity(itemString)
    local _, totalInvested = InvestmentTracker:GetInvested(link)
    local target = amount * numOwned

    InvestmentTracker:SubtractInvested(link, totalInvested - target)
end
