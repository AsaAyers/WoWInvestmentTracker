local InvestmentTracker = select(2, ...)
local Destroy = InvestmentTracker:NewModule("Destroy", 'AceBucket-3.0', 'AceEvent-3.0', 'AceTimer-3.0')


function Destroy:OnInitialize()
    self:RegisterEvent("UNIT_SPELLCAST_START")
    self:RegisterEvent("UNIT_SPELLCAST_INTERRUPTED")
    self:RegisterEvent("LOOT_CLOSED")
    self:RegisterEvent("UNIT_INVENTORY_CHANGED")
end

function Destroy:UNIT_SPELLCAST_START(event, target, spell)
    if target == "player" and spell == "Milling" or spell == "Prospecting" or spell == "Disenchanting" then
        self:Debug(event .. " " .. spell)
        self.lastScan = self:ScanBags()
        self.lootClosed = false
    end
end

function Destroy:UNIT_SPELLCAST_INTERRUPTED(event, target, spell)
    if self.lastScan ~= nil and target == "player" and spell == "Milling" or spell == "Prospecting" or spell == "Disenchanting" then
        self:Debug(event .. " " .. spell)
        self.lastScan = nil
    end
end

function Destroy:LOOT_CLOSED()
    if self.lastScan ~= nil then
        self.lootClosed = true
    end
end


function Destroy:UNIT_INVENTORY_CHANGED()
    if self.lootClosed and self.lastScan ~= nil then
        self:Debug('CompareScans')
        closedScan = self:ScanBags()
        self:CompareScans(self.lastScan, closedScan)
        self.lastScan = nil
    end
end
