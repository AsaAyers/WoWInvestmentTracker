## Interface: 60200
## Title: Investment Tracker
## Notes: Keeps track of how much gold you have invested in every item type
## SavedVariables: investedDB
## Dependency: TradeSkillMaster, TradeSkillMaster_Accounting
## Version: @1.0.0

libs\LibStub\LibStub.lua
libs\AceAddon-3.0\AceAddon-3.0.xml
libs\AceConsole-3.0\AceConsole-3.0.xml
libs\AceEvent-3.0\AceEvent-3.0.xml
libs\AceBucket-3.0\AceBucket-3.0.xml
libs\AceTimer-3.0\AceTimer-3.0.xml


#@do-not-package@
Tests\TestRunner.lua
#@end-do-not-package@


Modules\Tracker.lua
#@do-not-package@
Tests\Tracker.lua
#@end-do-not-package@

InvestmentTracker.lua
Modules\Options.lua
Modules\Crafting.lua
Modules\Destroy.lua
Modules\Convertables.lua
Modules\Auctions.lua
Modules\Mail.lua
Modules\Inventory.lua
Modules\Commands.lua
