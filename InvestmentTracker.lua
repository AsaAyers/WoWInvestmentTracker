 -- GLOBALS: LibStub, ReloadUI, TSMAPI, tinsert
local InvestmentTracker = select(2, ...)

--@debug@
function Dump(table)
    if not DevTools_DumpCommand then
        hash_SlashCmdList["/DUMP"]("DevTools_DumpCommand")
    end
    tempTable = table
    DevTools_DumpCommand('tempTable')
end
--@end-debug@

local prototype = {}
function prototype:Debug(...)
    --@debug@
    self:Print(...)
    --@end-debug@
end
function prototype:Log(...)
    self:Print(...)
end

function prototype:ToggleEvents(ENABLE_EVENT, DISABLE_EVENT)
    local enable
    local function disable(...)
        self:UnregisterAllEvents()
        if self[DISABLE_EVENT] ~= nil then
            self[DISABLE_EVENT](self, ...)
        end

        self:RegisterEvent(ENABLE_EVENT, enable)
    end
    enable = function (...)
        self:RegisterEvent(DISABLE_EVENT, disable)
        if self[ENABLE_EVENT] ~= nil then
            self[ENABLE_EVENT](self, ...)
        end
    end

    disable()
end

function prototype:ScanBags()
    local items = {}
    for _, _, itemString, qty in TSMAPI.Inventory:BagIterator(true) do
        if items[itemString] == nil then
            items[itemString] = 0
        end
        items[itemString] = items[itemString] + qty
        local link = TSMAPI.Item:ToItemLink(itemString)
    end
    return items
end


local function diff(before, after)
    local diff = {}
    local change
    for itemString, qty in pairs(after) do
        if before[itemString] == nil then
            diff[itemString] = after[itemString]
        else
            change = after[itemString] - before[itemString]
            if change ~= 0 then
                diff[itemString] = change
            end
        end
    end
    for itemString, qty in pairs(before) do
        if after[itemString] == nil then
            diff[itemString] = 0 - qty
        else
            change = after[itemString] - before[itemString]
            if change ~= 0 then
                diff[itemString] = change
            end
        end
    end
    return diff
end

function prototype:CompareScans(lastScan, current)
    local diff = diff(lastScan, current)

    local reagents = {}
    local targets = {}
    local reagentCount = 0
    local targetCount = 0
    for itemString, change in pairs(diff) do
        local link = TSMAPI.Item:ToItemLink(itemString)

        if change < 0 then
            reagents[link] = 0 - change
            reagentCount = reagentCount + 1
        else
            targets[link] = change
            targetCount = targetCount + 1
        end
    end

    if targetCount > 0 and reagentCount > 0 then
        InvestmentTracker:ConvertValue(reagents, targets)
    end
end

InvestmentTracker.Debug = prototype.Debug
InvestmentTracker.Log = prototype.Log

InvestmentTracker = LibStub("AceAddon-3.0"):NewAddon(InvestmentTracker, "InvestmentTracker", "AceBucket-3.0", "AceEvent-3.0", "AceConsole-3.0")
InvestmentTracker:SetDefaultModuleLibraries('AceConsole-3.0', 'AceEvent-3.0')
InvestmentTracker:SetDefaultModulePrototype(prototype)


--@debug@
_G['InvestmentTracker'] = InvestmentTracker
--@end-debug@

function InvestmentTracker.Debug(...)
    --@debug@
    InvestmentTracker:Print(...)
    --@end-debug@
end

local defaults = {
    global = {
        itemNames = {}
    },
    factionrealm = {
        items = {},
    },
}


local tooltipDefaults = {
}
function InvestmentTracker:OnInitialize()
    self.db = LibStub("AceDB-3.0"):New("investedDB", defaults, true)
    --@debug@
    self:RegisterChatCommand("r", ReloadUI)
    --@end-debug@

    -- TSMAPI.Inventory:RegisterCallback
    self.priceSources = {
        {
            key = "Invested",
            label = "How much money you have invested in that item type",
            callback = "GetInvestedEach",
            takeItemString = true
        },
    }
    self.Options = self:GetModule('Options')
    self.tooltip = { callbackLoad = "LoadTooltip", callbackOptions = "Options:LoadTooltipOptions", defaults = tooltipDefaults }
    TSMAPI:NewModule(self)
end

local function MoneyFormat(value, moneyCoins)
    return TSMAPI:MoneyToString(value, "|cffffffff", "OPT_PAD", moneyCoins and "OPT_ICON" or nil)
end

-- This is an experiment and may not be the best way to calculate this. I'm not
-- sure if I'll keep it, but it seems like a good starting point for guessing
-- the user's invested amount.
local function AccountingInvested(itemString)
        local Accounting = LibStub("AceAddon-3.0"):GetAddon("TSM_Accounting")
        local numOwned = TSMAPI.Inventory:GetTotalQuantity(itemString)

        if numOwned == 0 then
            return 0
        end

        local avgBuy, buyNum = Accounting:GetAvgBuyPrice(itemString)
        local avgSell, sellNum = Accounting:GetAvgSellPrice(itemString)
        if avgBuy == nil then avgBuy = 0; buyNum = 0; end
        if avgSell == nil then avgSell = 0; sellNum = 0; end

        if buyNum - sellNum > 0 then
            local investedEach = ((avgBuy * buyNum) - (avgSell * sellNum)) / (buyNum - sellNum)
            return investedEach -- * numOwned
        end
        return 0
end

function InvestmentTracker:GetInvestedEach(link)
    local investedEach = self:GetInvested(link)
    return investedEach
end

function InvestmentTracker:FindByName(name)
    return self.db.global.itemNames[name]
end

function InvestmentTracker:GetInvested(link)
    local itemString = TSMAPI.Item:ToBaseItemString(link)

    if self.db.factionrealm.items[itemString] == nil then
        local name = TSMAPI.Item:GetInfo(itemString)
        -- self:Debug('name', name)
        self.db.global.itemNames[name] = itemString

        local total = 0 -- AccountingInvested(itemString)
        if total < 0 then total = 0 end
        self.db.factionrealm.items[itemString] = total
    end
    local totalInvested = self.db.factionrealm.items[itemString]

    local numOwned = TSMAPI.Inventory:GetTotalQuantity(itemString)

    local investedEach
    -- I'm not sure what should happen if you run out of an item but still have
    -- gold invested.
    if (numOwned > 0) then
        investedEach = totalInvested / numOwned
    else
        investedEach = totalInvested
    end

    return investedEach, totalInvested, itemString
end

function InvestmentTracker:AddInvested(link, value)
    local _, totalInvested, itemString = self:GetInvested(link)
    self:Log('Adding', MoneyFormat(value), 'to', link)

    self.db.factionrealm.items[itemString] = totalInvested + value
end

function InvestmentTracker:SubtractInvested(link, value)
    local _, totalInvested, itemString = self:GetInvested(link)

    if value > totalInvested then
        value = totalInvested
    end

    if value > 0 then
        self:Log('Subtracting', MoneyFormat(value), 'from', link)

        local total = totalInvested - value

        self.db.factionrealm.items[itemString] = total
    elseif value < 0 then
        self:Log('Adding', MoneyFormat(0 - value), 'to', link)
        local total = totalInvested - value

        self.db.factionrealm.items[itemString] = total
    end
end

function numPairs(table)
    local count = 0
    for _ in pairs(table) do
        count = count + 1
    end
    return count
end

function InvestmentTracker:ConvertValue(fromItems, toItems)
    local totalValue = 0
    for link, count in pairs(fromItems) do
        local _, totalInvested = self:GetInvested(link)

        local numOwned = count + TSMAPI.Inventory:GetTotalQuantity(link)
        local investedEach = totalInvested / numOwned

        local reagentValue = count * investedEach
        totalValue = totalValue + reagentValue
        if reagentValue > 0 then
            self:SubtractInvested(link, reagentValue)
        end
    end

    local destination = ""
    local newItems = 0
    for link, count in pairs(toItems) do
        destination = destination .. link
        newItems = newItems + count
    end

    if totalValue > 0 then
        self:Log('Transferring value to:', destination)

        local valuePerItem = totalValue / newItems
        for link, count in pairs(toItems) do
            self:AddInvested(link, valuePerItem * count)
        end
    else
        self:Log('No value to transfer for:', destination)
    end
end

function InvestmentTracker:LoadTooltip(itemString, quantity, options, moneyCoins, lines)
    if not itemString then return end
    local numStartingLines = #lines

    if options.invested then
        local investedEach = self:GetInvested(itemString)

        if investedEach > 0 then
            local rightStr = MoneyFormat(investedEach * quantity, moneyCoins)
            tinsert(lines, numStartingLines+1, {left="Invested:", right=rightStr})
        end
    end

    if #lines > numStartingLines then
        tinsert(lines, numStartingLines+1, {left="|cffffff00InvestmentTracker:|r", right=""})
    end
    -- self:Debug('LoadTooltip', itemString, quantity, options.invested, moneyCoins)
end
