local AddonName, InvestmentTracker = ...
local BaseTracker = InvestmentTracker.Tracker

local LeftShark = "\124cff0070dd\124Hbattlepet:1687:6:3:422:70:64:0000000000000000\124h[Left Shark]\124h\124r"
local WoolCloth = "\124cffffffff\124Hitem:2592:0:0:0:0:0:0:0:0:0:0\124h[Wool Cloth]\124h\124r"
local LightLeather = "\124cffffffff\124Hitem:2318:0:0:0:0:0:0:0:0:0:0\124h[Light Leather]\124h\124r"
local LightArmorKit = "\124cffffffff\124Hitem:2304:0:0:0:0:0:0:0:0:0:0\124h[Light Armor Kit]\124h\124r"

local NameMap = {}
local function CaptureLinkName(link)
    -- Send a request for the item info to avoid [Unknown Item] links.
    GetItemInfo(link)
    local itemString = TSMAPI.Item:ToBaseItemString(link, true)
    local _, _, name = string.find(link, "%[(.*)%]")
    NameMap[itemString] = name
end
CaptureLinkName(LeftShark)
CaptureLinkName(WoolCloth)
CaptureLinkName(LightLeather)
CaptureLinkName(LightArmorKit)

local test = LibStub('TestRunner-1.0'):NewSuite('Tracker', {
    bail = true
})

function Dump(table)
    if not DevTools_DumpCommand then
        hash_SlashCmdList["/DUMP"]("DevTools_DumpCommand")
    end
    tempTable = table
    DevTools_DumpCommand('tempTable')
end



local function MockBagScan(items)
    local scan = {}

    for link, qty in pairs(items) do
        itemString = TSMAPI.Item:ToBaseItemString(link, true)
        if not itemString then error('Invalid item' .. link) end
        if scan[itemString] == nil then
            scan[itemString] = 0
        end
        scan[itemString] = scan[itemString] + qty
    end
    return scan

end

function TrackerSetup(t, owned, invested)
    owned = owned or {}
    invested = invested or {}
    local tracker = BaseTracker:new{
        owned = owned,
        -- TSMAPI.Item:GetInfo(itemString) doesn't work on first login, so we need a
        -- mock that knows about the specific items used for testing.
        GetInfo = function(self, itemString)
            print('GetInfo', itemString, NameMap[itemString])
            return NameMap[itemString]
        end
    }
    function tracker:Log(...) end
    for link, total in pairs(invested) do
        tracker:AddInvested(link, total)
    end

    function tracker:Log(...)
        local logTable = {n=select('#', ...), ...}
        t:comment('LOG: ' .. table.concat(logTable, ' '))
    end
    return tracker
end

local g = 10000

test('OwnedMock', function (t)
    owned = { [LightLeather] = 10 }
    local tracker = TrackerSetup(t, owned)

    t:equal(tracker:GetNumOwned(LightLeather), 10, '10 Light Leather owned')
    t:equal(tracker:GetNumOwned(LightArmorKit), 0, '0 Light Armor Kits owned')
end)

test('GetInfo Mock', function(t)
    local owned = { [LightLeather] = 10 }
    local tracker = TrackerSetup(t, owned)

    t:equal(tracker:GetInfo('i:2318'), 'Light Leather')
    t:equal(tracker:GetInfo('i:2318'), 'Light Leather')
    t:equal(tracker:GetInfo('i:2318'), 'Light Leather')
    t:equal(tracker:GetInfo('i:2318'), 'Light Leather')
    t:equal(tracker:GetInfo('i:2318'), 'Light Leather')
    t:equal(tracker:GetInfo('i:2318'), 'Light Leather')

end)


test('New Item', function (t)
    local owned = { [LightLeather] = 10 }
    local tracker = TrackerSetup(t, owned)
    local each, total, string = tracker:GetInvested(LightLeather)

    t:equal(each, 0, '0c each invested in LightLeather')
    t:equal(total, 0, '0c total invested in LightLeather')
    t:equal(string, 'i:2318')
end)

test('GetInvested - 0 owned', function(t)
    local owned = {}
    local invested = { [LightLeather] =  10 * g}
    local tracker = TrackerSetup(t, owned, invested)

    local each, total, string = tracker:GetInvested(LightLeather)

    t:equal(each, 10 * g, '10g each invested in LightLeather')
    t:equal(total, 10 * g, '10g total invested in LightLeather')
    t:equal(string, 'i:2318')
end)

test('AddInvested', function(t)
    local owned = { [LightLeather] = 10 }
    local tracker = TrackerSetup(t, owned)

    tracker:AddInvested(LightLeather, 10 * g)

    local each, total, string = tracker:GetInvested(LightLeather)

    t:equal(each, 1 * g, '1g each invested in LightLeather')
    t:equal(total, 10 * g, '10g total invested in LightLeather')
    t:equal(string, 'i:2318')
end)


test('SubtractInvested', function(t)
    local owned = { [LightLeather] = 10 }
    local invested = { [LightLeather] =  10 * g}
    local tracker = TrackerSetup(t, owned, invested)

    tracker:SubtractInvested(LightLeather, 5 * g)

    local each, total, string = tracker:GetInvested(LightLeather)

    t:equal(each, 0.5 * g, '50s each invested in LightLeather')
    t:equal(total, 5 * g, '5g total invested in LightLeather')
    t:equal(string, 'i:2318')

end)


test('SubtractInvested - profit', function(t)
    local owned = { [LightLeather] = 10 }
    local invested = { [LightLeather] =  10 * g}
    local tracker = TrackerSetup(t, owned, invested)

    -- Subtract more value than was put in
    tracker:SubtractInvested(LightLeather, 15 * g)

    local each, total, string = tracker:GetInvested(LightLeather)

    t:equal(each, 0, '0g each invested in LightLeather')
    t:equal(total, 0, '0g total invested in LightLeather')
    t:equal(string, 'i:2318')

end)

test('Crafting', function(t)
    local owned = { [LightLeather] = 10 }
    local invested = { [LightLeather] =  10 * g}
    local tracker = TrackerSetup(t, owned, invested)

    local before = MockBagScan(tracker.owned)

    tracker.owned = {
        [LightLeather] = 9,
        [LightArmorKit] = 1,
    }

    local after = MockBagScan(tracker.owned)

    local reagents, targets = tracker:CompareScans(before, after)
    t:comment('reagents')
    Dump(reagents)
    t:comment(tostring(reagents))
    tracker:ConvertValue(reagents, targets)

    local leatherEach, leatherTotal = tracker:GetInvested(LightLeather)
    local armorKitEach, armorKitTotal = tracker:GetInvested(LightArmorKit)

    t:equal(tracker:GetNumOwned(LightLeather), 9, 'own 9 light leather')
    t:equal(leatherEach, 1 * g, '1g each in light leather')
    t:equal(leatherTotal, 9 * g, '9g total in light leather')

    t:equal(tracker:GetNumOwned(LightArmorKit), 1, 'own 1 Armor Kit')
    t:equal(armorKitEach, 1 * g, '1g each in armor kits')
    t:equal(armorKitTotal, 1 * g, '1g each in armor kits')

end)
