local MAJOR, MINOR = "TestRunner-1.0", 1
local TestRunner, oldminor = LibStub:NewLibrary(MAJOR, MINOR)

if not TestRunner then return end -- No Upgrade needed.

local tinsert, tconcat, tremove = table.insert, table.concat, table.remove

TestRunner.nextID = TestRunner.nextID or 1

local assert = {}
function assert:new(o)
    o = o or {}   -- create object if user does not provide one
    setmetatable(o, self)
    self.__index = self
    o._testCount = 0
    o._failCount = 0
    o._log = {}
    return o
end

function assert:_assert(ok, details)
    self._testCount = self._testCount + 1

    tinsert(self._log, ("%s %s %s"):format(
      ok and 'ok' or 'not ok',
      self._testCount,
      details.message
    ))

    if not ok then
        self._failCount = self._failCount + 1

        if details.extra then
            tinsert(self._log, (" ---\n %s\n ...\n"):format(details.extra))
        else
            tinsert(self._log, (" ---\n operator: %s\n expected: %s\n actual: %s\n ...\n"):format(
                details.operator,
                tostring(details.expected),
                tostring(details.actual)
            ))
        end



    end
end

function assert:equal(actual, expected, message)
    self:_assert(actual == expected, {
        operator = 'equal',
        message = message or 'should be equal',
        actual = actual,
        expected = expected,
    })
end

function assert:ok(actual, message)
    expected = true
    self:_assert(actual == expected, {
        operator = 'ok',
        message = message or 'should be true',
        actual = actual,
        expected = expected,
    })
end

function assert:fail(message)
    self:_assert(false, {
        operator = 'fail',
        message = message
    })
end

function assert:error(message, stack)
    self:_assert(false, {
        operator = 'error',
        message = message,
        extra = stack,
    })
end

function assert:comment(message)
    tinsert(self._log, ("# %s"):format( message ))
end

function TestRunner:NewSuite(name, options)
    options = options or {}
    local queue = {}
    local only = false

    -- self seems to be added because of the __call below.
    function PushTest(self, name, test)
        if only then return end

        tinsert(queue, { name = name, test = test })
    end
    local testapi = {
        only = (function(name, test)
            if only then
                error('You can only have one test.only')
            end

            wipe(queue)
            PushTest(name, text)
            only = true
        end),
        skip = (function(name)
        end)
    }
    setmetatable(testapi, { __call = PushTest })

    local suiteName = 'TestSuite:' .. name
    TestRunner.nextID = TestRunner.nextID + 1

    TestRunner = LibStub("AceAddon-3.0"):NewAddon(suiteName, "AceConsole-3.0")
    LibStub("AceAddon-3.0").addons[suiteName] = nil

    local t = assert:new()

    function TestRunner:OnEnable()
        local startStack = debugstack(1)

        local function handler(error)
            local stack = debugstack(2)
            local totalLength = string.len(stack)
            -- print(startStack)
            -- print(stack)
            local shortStack = string.sub(stack, 0, string.len(stack) - string.len(startStack))

            t:error(error, shortStack)
        end

        while(#queue > 0) do
            local tmp = tremove(queue, 1)

            t:comment('TEST ' .. tmp.name)
            local function TestRunner() tmp.test(t) end
            local testOK = xpcall(TestRunner, handler)

            if (t._failCount > 0 and options.bail) then
                break
            end
        end

        if t._failCount > 0 or t._testCount == 0 then
            self:ShowResults()
        else
            self:Print('|cFF00FF00All Tests Passed|r')
        end
    end

    function TestRunner:ShowResults()


        local AceGUI = LibStub("AceGUI-3.0")
        -- Create a container frame
        local f = AceGUI:Create("Frame")
        f:SetCallback("OnClose",function(widget)
            AceGUI:Release(widget)
            ReloadUI()
        end)
        f:SetTitle(name .. " Test Results")
        f:SetStatusText(self._failCount == 0 and 'ok' or 'not ok')
        f:SetLayout("Fill")
        local editbox = AceGUI:Create("MultiLineEditBox")
        editbox:SetLabel("Insert text:")
        editbox:SetWidth(200)
        editbox:DisableButton(true)
        f:AddChild(editbox)

        local button = AceGUI:Create("Button")
        button:SetText("Click Me!")
        button:SetWidth(200)
        f:AddChild(button)


        local allText = ''
        while(#t._log > 0) do
            allText = allText .. tremove(t._log, 1) .. '\n'
        end

        allText = allText .. ("\n1..%s\n# tests %s\n# pass %s\n# fail %s" ):format(
            t._testCount,
            t._testCount,
            t._testCount - t._failCount,
            t._failCount
        )

        editbox:SetText(allText)

    end

    return testapi
end
